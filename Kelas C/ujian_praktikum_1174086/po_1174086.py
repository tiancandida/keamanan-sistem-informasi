import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ujian_praktikum_1174086.settings')
import django
django.setup()
import random
from ujian_aplikasi_1174086.models import User
from faker import Faker

fakegen = Faker()
nakhir = ['Alvan','Gani','Arrizal','Bhakti','Reza','Chandra','Dirga']

def populate(N=30):
    for entry in range(0,N):
        fakefirst = fakegen.first_name()
        fakemail = fakegen.email()

        nawal = User.objects.get_or_create(firstname=fakefirst,lastname=random.choice(nakhir),usermail=fakemail)[0]

if __name__ == '__main__':
    inputan = int(input("Inputkan Angka = "))
    print("Tunggu Beberapa Saat ...")
    populate(inputan)
    print("Sukses")